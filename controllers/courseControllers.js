const Course = require("../models/Course.js");
const router = require("../routes/user.js");

// Create new course
/* 
    Steps:
    1. Create a new Course object using the mongoose model
    2. Save the new Course to the database
*/

module.exports.addCourse = (reqBody) =>{
    // Creates a new variable "newCourse" and instantiate a new Course object
    // Uses the info from the reques body to provide all necessary infoss
    /* 
        Alternate Solution:
        module.exports.addCourse = (data) => {
        //if the admin is true
        if(data.isAdmin){
                let newCourse = new Course({

                name : data.course.name,
			    description : data.course.description,
			    price : data.course.price
            })
            // saves the created object to our database
            return newCourse.save().then((course, error)=>{
                // course creation failed
                if(error){
                    return false;
                    // course creation succeed
                } else {
                    return true;           
                }
            })
        }else{
            return false;
        }
    };
    */
        let newCourse = new Course({
            name: reqBody.name,
            description: reqBody.description,
            price: reqBody.price
        })
        // saves the created object to our database
        return newCourse.save().then((course, error)=>{
            // course creation failed
            if(error){
                return false;
                // course creation succeed
            } else {
                return true;           
            }
        })
    }

// retrieve all courses

module.exports.getAllCourses = () =>{
    return Course.find({}).then(result => {return result})
}
    
// retrieve all active courses
module.exports.getAllActive = () => {
   return Course.find({isActive: true}).then(result => {return result})
}

// retrieve specific course
module.exports.getCourse = (reqParams) =>{
    console.log(reqParams)
    return Course.findById(reqParams.courseId).then(result =>{return result})
}

// update a course
/* 
    Steps:
        1. Create a variable "updatedCourse" which will contain the info retrieved from req body
        2. Find and update the course using the course ID retrieved from the req params and the variable "updateCourse"
*/

// info for updating will be coming from URL parameters and request body
module.exports.updateCourse = (reqParams, reqBody) => {

    // specify the fields of the document to be updated
    let updatedCourse = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    }
    // findByIdUpdate(document ID, updatesToBeApplied)
    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error)=>
    {
        // course not updated
        if(error){
            return false;
            // course updated successfully
        }else{
            return true;
        }
    })
}

//Archive a course
module.exports.archiveCourse = (reqParams, data) => {

    if(data.isAdmin){
        let archivedCourse = {
        isActive: data.course.isActive
    }
    // saves the created object to our database
    return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error)=>
    {
        // course not updated
        if(error){
            return false;
            // course updated successfully
        }else{
            console.log(course);
            return true;
        }
    })
}else{
    return false;
}
}