const express = require("express")
const router = express.Router();
const courseControllers = require("../controllers/courseControllers")
const auth = require("../auth.js");

//Create course
router.post("/", auth.verify, (req,res)=>{
    /* 
        Alternative:

        const data ={
            course: req.body,
            isAdmin: auth.decode(req.headers.authorization).isAdmin

            courseControllers.addCourse(req.body).then(resultFromController => res.send(resultFromController))
        }
    */
    const userData = auth.decode(req.headers.authorization)

    if(userData.isAdmin == true){
    courseControllers.addCourse(req.body).then(resultFromController => res.send(resultFromController))
    }else{
        return res.send(401, "Unauthorized")
    }
});

// Retrieve all courses
router.get("/all",(req, res)=>{
    courseControllers.getAllCourses().then(resultFromController => res.send(resultFromController))
})
/* 
    Mini-Activity:
        1. Create a route that will retrieve ALL ACTIVE courses (endpoint: "/")
        2. No Need for user to login
        3. Create a controller that wil return ALL ACTIVE courses
*/
// Retrieve all active courses
router.get("/",(req, res)=>{
    courseControllers.getAllActive().then(resultFromController => res.send(resultFromController))
})

// Retrieve a specific course
router.get("/:courseId",(req,res)=>{

    courseControllers.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})
// Update a course
router.put("/:courseId", auth.verify, (req,res)=>{

    courseControllers.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})
// Archive a course
router.put("/:courseId/archive", auth.verify, (req,res)=>{

    const data ={
        course: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    courseControllers.archiveCourse(req.params, data).then(resultFromController => res.send(resultFromController))
})

//export the router object for index.js file
module.exports = router;